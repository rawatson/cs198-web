'use strict';

var React  = require('react');
var Router = require('react-router');
var Link = Router.Link;

module.exports = React.createClass({
    render: function() {
        return (
            <div className="helper-queue">
                <Link to="/">Home!</Link>
                <p>404!</p>
            </div>
        );
    }
});
