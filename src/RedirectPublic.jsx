'use strict';

var React = require('react');

module.exports = React.createClass({
    render: function() {
        return (
            <meta httpEquiv="refresh" content="0;URL=/" />
        );
    }
});
