'use strict';

// React/routing dependencies
var React       = require('react');
var Router      = require('react-router');
var Route       = Router.Route;
var RouteHandler  = Router.RouteHandler;
var NotFoundRoute   = Router.NotFoundRoute;
var DefaultRoute  = Router.DefaultRoute;
var Link      = Router.Link;

// Components
var HelperQueue   = require('./HelperQueue');
var Paperless     = require('./Paperless');
var NotFound    = require('./NotFound.jsx');
var RedirectPublic  = require('./RedirectPublic.jsx');
var Sandbox     = require('./Sandbox/index.jsx');

var api       = require('./shared/cs198.js');
var timezone    = 'America/Los_Angeles';


var App = React.createClass({
  // TODO: don't expose links to go between each view; password protect
  render: function() {
    return <RouteHandler />
  }
});

var routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="queue" handler={HelperQueue.HelperView} />
    <Route name="signup" handler={HelperQueue.SignupView} />
    <Route name="shifts" handler={HelperQueue.ShiftsView(timezone)} />
    <Route name="sandbox" handler={Sandbox} />
    <Route name="create" path="shifts/create"
      handler={HelperQueue.CreateShiftsView(timezone)} />
    <Route path="shifts/swap"
      handler={HelperQueue.SelectSwapType(timezone)} />
    <Route path="shifts/swap/one_for_one"
      handler={HelperQueue.SwapShiftsView(timezone)} />
    <Route path="shifts/swap/cover"
      handler={HelperQueue.CoverShiftView(timezone)} />
    <Route path="shifts/swap/permanent"
      handler={HelperQueue.PermanentSwapView(timezone)} />

    <Route name="paperless_section" path="paperless/section"
      handler={Paperless.Section} />
    <Route name="paperless_assignment" path="paperless/assignment"
      handler={Paperless.Assignment} />
    <DefaultRoute handler={RedirectPublic} />
    <NotFoundRoute handler={NotFound} />
  </Route>
);

Router.run(routes, Router.HistoryLocation, function(Handler) {
  React.render(<Handler />, document.body);
});
