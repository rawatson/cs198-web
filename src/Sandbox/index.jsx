'use strict';

var React   = require('react');
var moment  = require("moment");
var _       = require("underscore");

var api = require('../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'Sandbox',
  render: function() {
    var makeRequest = e => {
      e.preventDefault();
      api.Sandbox.test().then((res) => {
        console.log(res);
        alert("Congratulations, you made a req while authenticated!");
      });
    }

    return (
      <div className="helper-queue">
        <header className="row title">
          <h1>Sandbox Page!</h1>
        </header>
        <p>
          Welcome to the sandbox!  This file isn't used for anything, so feel free to use it to test new code.
        </p>
        <p>
          <button onClick={makeRequest} type="button" className="btn btn-default" ref="theImportantButton">
            <a href="#"> Click me to trigger a protected request!</a>
          </button>
        </p>
      </div>
    );
  }
});
