import FluxStore from './FluxStore.jsx';
import PaperlessSectionDispatcher from '../dispatchers/PaperlessSectionDispatcher.jsx';
import PaperlessSectionActionTypes from '../constants/PaperlessSectionConstants.jsx';
import api from '../../shared/cs198.js';
import DataTools from '../../shared/DataTools.js';

let _store = null;

/* Temporary, define it in CS198 API later. Section is ignored for now but will be used by API*/
var backend_old_assignments = function(section) {
  return(
    [{assignment_number: 1,
      assignment_name: 'Game of Life',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : true,
                  functionality: '-', style: '-', late_days: '0'}]},
     {assignment_number: 2,
      assignment_name: 'ADTs',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : false,
                  functionality: '+', style: 'V+', late_days: '0'}]}, 
     {assignment_number: 3,
      assignment_name: 'Recursion',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : true,
                  functionality: null, style: '+', late_days: '0'}]}, 
     {assignment_number: 4,
      assignment_name: 'Boggle',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : false,
                  functionality: 'V', style: 'V+', late_days: '0'}]}]);
}

/* Temporary, define it in CS198 API later. Section is ignored for now but will be used by API*/
var backend_new_assignments = function(section) {
  return( 
    [{assignment_number: 4,
      assignment_name: 'Boggle',
      students: [{first_name : 'Kevin', last_name : 'Shin',
                  sunetid: 'hgkshin', released : true,
                  functionality: 'V+', style: 'V+', late_days: '0'}]},
     {assignment_number: 2,
      assignment_name: 'ADTs',
      students: [{first_name : 'Kevin', last_name : 'Shin',
                  sunetid: 'hgkshin', released : false,
                  functionality: '+', style: 'V+', late_days: '0'}]}]);
}

class PaperlessSectionStore extends FluxStore {     
  constructor() {
    super();
  }
  
  /* Attempts to match based on course_relation_id, returns 0 if not found */
  findSectionIndex(sections, course_relation_id) {
    if (course_relation_id === null) {
      return 0;
    }
    for (var i = 0; i < sections.length; i++) {
      if (sections[i].section_info.id === course_relation_id) {
        return i;
      } 
    } 
    // Wrong input results in most recent course
    return 0;   
  }

  initializePaperlessSectionState(auth_sunetid, view_sunetid, course_relation_id) {
    return new Promise(function(resolve, reject) {
      _store = {};
      if (view_sunetid === undefined) {
        view_sunetid = auth_sunetid;
      }
      Promise.all([api.People.find(auth_sunetid),
                   api.People.find(view_sunetid)])
      .then(results => {
        _store.auth_person = results[0].data;
        _store.view_person = results[1].data;
        return Promise.all([api.Courses.show(_store.view_person.id),
                            api.Staff.show(_store.view_person.id)]);
      }).then(results => {
        _store.sections = [] 
        var student_history = results[0].data;
        var staff_history = results[1].data;
        for (var i = 0; i < student_history.length; i++) {
          _store.sections.push({relation : 'Student', section_info: student_history[i]});
        }
        for (var i= 0; i < staff_history.length; i++) {
          _store.sections.push({relation : staff_history[i].position,
                                section_info: staff_history[i]});
        }
        _store.sections.sort(DataTools.sectionComparison);
        console.log(_store.sections);
        _store.selected_section = null;
        if (_store.sections.length > 0) {
          var section_index = this.findSectionIndex(_store.sections, course_relation_id);
          _store.selected_section = _store.sections[section_index];
        }
        _store.assignments = backend_old_assignments(_store.selected_section);
        resolve(null);
      });
    }.bind(this));
  }  
  
  /* Initializes store if null, otherwise just returns the store */
  getPaperlessSectionState() {
    _store.assignments.sort((a, b) => -(a.assignment_number - b.assignment_number));
    return _store;
  }
};

let PLStoreInstance = new PaperlessSectionStore();

PLStoreInstance.dispatchToken = PaperlessSectionDispatcher.register(action => {
  var data = action.action.data;
  switch (action.action.type) {
    /* Updates the selected section, its assignments,
     * and the default selected assignment to display. */
    case PaperlessSectionActionTypes.SELECT_NEW_SECTION:
      _store.selected_section = data.section;
      _store.assignments = backend_new_assignments(_store.selected_section); 
      break;
    default:
      return;
  }
  PLStoreInstance.emitChange();
});

export default PLStoreInstance;
