'use strict';

var React  = require('react');
var moment = require('moment');
var api    = require('../../shared/cs198.js')
var RB = require('react-bootstrap');

`
module.exports = class StudentTableViewComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  
  releasedCheckBox(released) {
    if (released) {
      return(<a href="#"><RB.Glyphicon glyph='ok' style={{color:'green'}}/></a>);
    }
    return(<a href="#"><RB.Glyphicon glyph='remove' style={{color:'red'}}/></a>);
  }

  renderTableBody = () => {
    return null; // TODO: enable
    return this.props.assignment.students.map(student =>
      <tr key={student.sunetid}>
        <td className='text-center'> <a href='#'><RB.Glyphicon glyph='edit' /></a> </td>
        <td className='text-center'>
          {student.first_name + ' ' + student.last_name +
           ' (' + student.sunetid + ')'}
        </td>
        <td className='text-center'> <a href='#'><RB.Glyphicon glyph='download' /></a> </td>
        <td className='text-center'> {1} </td>
        <td className='text-center'> {student.functionality} </td>
        <td className='text-center'> {student.style} </td>
        <td className='text-center'> {student.late_days} </td>
        <td className='text-center'> {this.releasedCheckBox(student.released)}  </td>
      </tr>
    )
  }

  render() {
    return (
      <RB.Table responsive hover striped bordered>
        <thead>
          <tr>
            <th className='text-center'> Edit Grade</th>
            <th className='text-center'> Student </th>
            <th className='text-center'> Download</th>
            <th className='text-center'> Submissions</th>
            <th className='text-center'> Functionality </th>
            <th className='text-center'> Style </th>
            <th className='text-center'> Late Days </th>
            <th className='text-center'> Grade Released </th>
          </tr>
        </thead>
        <tbody>
          {this.renderTableBody()}
        </tbody>
      </RB.Table>
    );
  } 
}
`
