'use strict';

var React  = require('react');
var moment = require("moment");
var api    = require('../../shared/cs198.js')
var RB = require('react-bootstrap');

var api = require('../../shared/cs198.js');
var HeaderView = require('./Header.jsx');
var PaperlessSectionStore = require('../stores/PaperlessSectionStore.jsx');
var PaperlessSectionActions = require('../actions/PaperlessSectionActions.jsx');
var DataTools = require('../../shared/DataTools.js');

module.exports = class AssignmentViewComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = null;
    /* Log in to get sunet ID */
    api.Auth.login().then((res) => {
      this.state = {};
      /* Used to initialize Paperless store for header */
      var auth_sunetid = 'hgkshin';
      var sl_sunetid = props.query.sl_sunetid;
      var course_relation_id = parseInt(props.query.course_relation_id);
      PaperlessSectionStore.initializePaperlessSectionState(auth_sunetid, sl_sunetid, course_relation_id)
      .then(() => {
        this.setState(PaperlessSectionStore.getPaperlessSectionState());
      }).then(() => {
        /* Used to initialize Assignment store */ 
        var student_sunetid = props.query.student_sunetid;
        var assignment_id = parseInt(props.query.assignment_id);
        var submission_id = parseInt(props.query.submission_id);
      });
    });
  }

  renderAssignment() {
    return (  
      <div>
        <RB.Panel>
        <RB.Input wrapperClassName='wrapper'>
          <RB.Row>
            <RB.Col xs={3}>
              <select type='select' className='form-control'>
                <option value='' disabled selected>Student</option>
                <option value='other'>...</option>
              </select>
            </RB.Col>
            <RB.Col xs={4}>
              <select type='select' className='form-control'> 
                <option value='' disabled selected>Assignment</option>
                <option value='other'>...</option>
              </select>
            </RB.Col>
            <RB.Col xs={4}>
              <select type='select' className='form-control'>
                <option value='' disabled selected>Submission</option>
                <option value='other'>...</option>
              </select>
            </RB.Col>
            <RB.Col xs={1}>
              <RB.ButtonInput type="submit" value="Go" bsStyle={null} bsSize="small" disabled={true} />
            </RB.Col>
          </RB.Row>
        </RB.Input>

        <RB.PageHeader>
          <p> Assignment 3: Recursion </p>
          <small>
            <p>CS106X Winter 2015 | SL Kevin Shin (hgkshin) </p>
            <p>Student Reid Watson (rawatson) </p>
          </small>
        </RB.PageHeader>
                  <div>
          <RB.TabbedArea defaultActiveKey={1} animation={false}>
          <RB.TabPane key={1} eventKey={1} tab={'Heap-Priority.cpp'}/>
            <RB.Panel>
              
            </RB.Panel>
          <RB.TabPane key={2} eventKey={1} tab={'1Heap-Priority.cpp'}/>
          <RB.TabPane key={3} eventKey={1} tab={'eagelajgealkgjea.cpp'}/>
          <RB.TabPane key={4} eventKey={1} tab={'aegleajgleajglea.cpp'}/>
          <RB.TabPane key={5} eventKey={1} tab={'aelvjealkjeaklgjae.cpp'}/>
          <RB.TabPane key={6} eventKey={1} tab={'aegkleajgaeklgae.cpp'}/>
          <RB.TabPane key={7} eventKey={1} tab={'aegealkgjeaga.cpp'}/>
          <RB.TabPane key={8} eventKey={1} tab={'1eaaegaegae.cpp'}/>
          <RB.TabPane key={9} eventKey={1} tab={'1aegljealkgjeagklae.cpp'}/>
          <RB.TabPane key={10} eventKey={1} tab={'1eaklgjealkgajeg.cpp'}/>
          <RB.TabPane key={111} eventKey={1} tab={'1eageajglkaegaega.cpp'}/>
          <RB.TabPane key={112} eventKey={1} tab={'1aegeajgealk.cp'}/>
          </RB.TabbedArea>  
        </div>
        </RB.Panel>
      </div>
    );
  }

  render() { 
    // Slight hack around asynchronous initialization 
    if (this.state === null) {
      return (<div> </div>);
    } else if (this.state.selected_section.relation !== 'Section Leader') {
      return (<div> <HeaderView />
          {'Your current view is ' + this.state.selected_section.relation + ' (not an SL) and Kevin has been too lazy to implement that view. Give it some time homie.'} </div>);
    }
    console.log(this.state.selected_section.relation);
    return (
        <div>
          <HeaderView />
          <div>
            {this.renderAssignment()}
          </div>
        </div>
    );
  }
}

