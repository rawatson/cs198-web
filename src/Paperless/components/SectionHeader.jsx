'use strict';

var React            = require('react');
var moment           = require('moment');
var api              = require('../../shared/cs198.js')
var DataTools        = require('../../shared/DataTools.js')
var RB               = require('react-bootstrap');

var PaperlessSectionStore   = require('../stores/PaperlessSectionStore.jsx');
var PaperlessSectionActions = require('../actions/PaperlessSectionActions.jsx');

`
module.exports = class PaperlessHeaderViewComponent extends React.Component {
  static propTypes = {
    auth_person: React.PropTypes.object.isRequired,
    view_person: React.PropTypes.object.isRequired,
    sections: React.PropTypes.array.isRequired
  }

  constructor(props) {
    super(props);
  }

  changeView(e) {
    /* TODO: Error check and validate SUNet ID for better UX*/
    var new_sunet = this.refs.sunetid.refs.input.getDOMNode().value;
    window.location.href = "/paperless/section?sunetid=" + new_sunet;
  }
  
  render() {
    var auth_person = this.props.auth_person;
    var view_person = this.props.view_person;
   
    const goButton = (<RB.Button type="submit" onClick={this.changeView.bind(this)}>
                         Go
                       </RB.Button>);
    return (
        <RB.Navbar brand={<a href="/paperless/section">Paperless</a>}>
          <RB.Nav>
            <RB.NavItem disabled>
              {'Logged in as ' + DataTools.personToString(auth_person)}
            </RB.NavItem>
          </RB.Nav>
          <RB.Nav navbar right>

          <RB.DropdownButton className='scrollable-menu'
                             title='Change Section View'>
            <RB.MenuItem divider />
            <RB.MenuItem header className='text-center'>
              {'Search for Different SL'}
            </RB.MenuItem> 
            <RB.MenuItem>
              <form>
                <RB.Input name='sunetid'
                 placeholder={'SUNet (e.g. ' + view_person.sunetid  +')'}
                 type='text' bsSize='small' ref='sunetid' buttonAfter={goButton}/>
              </form>
            </RB.MenuItem>

            <RB.MenuItem divider />
            <RB.MenuItem header className='text-center'>
              {'Find History for ' + DataTools.personToString(view_person)}
            </RB.MenuItem> 
            {this.props.sections.map((section) =>
                <RB.MenuItem key={section.section_info.id}
                    href={'/paperless/section?sunetid=' + view_person.sunetid +
                          '&course_relation_id=' + section.section_info.id}
                    eventKey={section}>
                   {DataTools.sectionInformationToDiv(section)}
                </RB.MenuItem>)} 
            </RB.DropdownButton>
          </RB.Nav>
        </RB.Navbar>
    );
  }
}

`
