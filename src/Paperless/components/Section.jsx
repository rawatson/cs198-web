'use strict';

var React  = require('react');
var moment = require('moment');
var api    = require('../../shared/cs198.js')
var RB = require('react-bootstrap');

var SectionHeaderView = require('./SectionHeader.jsx');
var SubmissionsTableView = require('./SubmissionsTable.jsx');

var DataTools = require('../../shared/DataTools.js');

//TODO: REmove
var backend_old_assignments = function(section) {
  return(
    [{assignment_number: 1,
      assignment_name: 'Game of Life',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : true,
                  functionality: '-', style: '-', late_days: '0'}]},
     {assignment_number: 2,
      assignment_name: 'ADTs',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : false,
                  functionality: '+', style: 'V+', late_days: '0'}]},
     {assignment_number: 3,
      assignment_name: 'Recursion',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : true,
                  functionality: null, style: '+', late_days: '0'}]},
     {assignment_number: 4,
      assignment_name: 'Boggle',
      students: [{first_name : 'Reid', last_name : 'Watson',
                  sunetid: 'rawatson', released : false,
                  functionality: 'V', style: 'V+', late_days: '0'}]}]);
}

`
module.exports = class SectionComponent extends React.Component {

  /*
   *
   * State initialization functions
   *
   */

  initializeAssignments = (state, view_person, course_relation_id) => {
    return Promise.all([
      api.People.sections.index(view_person.sunetid, course_relation_id),
      api.Assignments.index(course_relation_id),
      api.AssignmentGrades.index(course_relation_id)
    ]).then( res => {
      state.students = res[0].data;
      state.assignments = res[1].data;
      state.assignment_grades = res[2].data;

      return state;
    });
  }

  initializeViewPersonRelations = (state, student_history, staff_history, course_relation_id) => {
    state.sections = [];
    for (var student of student_history) {
      state.sections.push({relation : 'Student', section_info: student});
    }
    for (var staff of staff_history) {
      state.sections.push({relation : staff.position, section_info: staff});
    }
    state.sections.sort(DataTools.sectionComparison);

    if (state.sections.length > 0) {
      var section_index = this.findSectionIndex(state.sections, course_relation_id);
      state.selected_section = state.sections[section_index];
    }
    return state;
  }

  initializeViewPersonPromise = (view_sunetid, auth_person) => {
    if (view_sunetid === undefined || view_sunetid === auth_person.sunetid) {
      return Promise.resolve({data: auth_person});
    } else {
      return api.People.find(view_sunetid);
    }
  }

  initializeState = (auth_person, view_sunetid, course_relation_id) => {
    let state = {auth_person: auth_person};

    return this.initializeViewPersonPromise(view_sunetid, auth_person)
    .then(results => {
      state.view_person = results.data;
      return Promise.all([api.Courses.show(state.view_person.id),
                          api.Staff.show(state.view_person.id)]);
    }).then(results => {
      var student_history = results[0].data;
      var staff_history = results[1].data;

      state = this.initializeViewPersonRelations(state, student_history, staff_history, course_relation_id);
      course_relation_id = state.selected_section.section_info.course.id;
      return this.initializeAssignments(state, state.view_person, course_relation_id);
    }).then(state => {
      this.setState(state);
    });
  }

  constructor(props) {
    super(props);

    api.People.getAuthenticatedPerson().then((auth_person) => {
      var course_relation_id = parseInt(props.query.course_relation_id);
      this.initializeState(auth_person, props.query.sunetid, course_relation_id);
    });
  }

  /*
   *
   * Utility functions
   *
   */

  /* Attempts to match based on course_relation_id, returns 0 if not found */
  findSectionIndex = (sections, course_relation_id) => {
    if (!course_relation_id) {
      for (var i = 0; i < sections.length; i++) {
        var type = sections[i].relation;
        var isGraderRelation = type == 'Section Leader' || type =='Grader' || type == 'Course Helper';
        if (!isGraderRelation)
          continue;
        return i;
      }
      return 0;
    }

    for (var i = 0; i < sections.length; i++) {
      if (sections[i].section_info.id === course_relation_id) {
        return i;
      }
    }
    // Wrong input results in most recent course
    return 0;
  }


  /* If all assignments are graded, returns newest assignment.
   * If there are multiple ungraded assignments, returns oldest one. */
  get_oldest_ungraded_assignment = (assignments) => {
    if (assignments.length === 0) {
      return null;
    }

    let ungraded_assignment = null;
    let newest_assignment = null;
    for (let assignment of assignments) {
      if (newest_assignment === null || assignment.assignment_number > newest_assignment) {
        newest_assignment = assignment.assignment_number;
      }
      // TODO: handle grade information
    }
    ungraded_assignment = (ungraded_assignment === null) ? newest_assignment : ungraded_assignment;
    return ungraded_assignment;
  }


  /*
   *
   * Rendering
   *
   */
  renderDownloadButtons() {
    return (
      <div className='btn-toolbar pull-right'>
          <RB.Button bsStyle='primary'>
            {'Download All '}<RB.Glyphicon glyph='download' />
          </RB.Button>
      </div>
    );
  }

  renderAssignmentSectionHeader(assignment, selected_section) {
    var course = selected_section.section_info.course;
    return (
      <RB.PageHeader>
        <p> {assignment.assignment_name} </p>
        <small>
          <p>
            {course.department_code.toUpperCase() +
            course.course_number.toUpperCase() +
             ' ' + course.quarter.quarter_name +
             ' ' + course.quarter.year}
          </p>
          <p>
            {'Section Leader: ' + DataTools.personToString(this.state.view_person)}
          </p>
        </small>
      </RB.PageHeader>
    );
  }

  renderAssignment(assignment) {
    var selected_section = this.state.selected_section;
    return (
      <RB.TabPane
       tab={'Assignment ' + assignment.assignment_number}
       key={assignment.assignment_number}
       eventKey={assignment.assignment_number}>
        <RB.Panel>
          {this.renderDownloadButtons()}
          {this.renderAssignmentSectionHeader(assignment, selected_section)}
          <SubmissionsTableView assignment={assignment}/>
        </RB.Panel>
      </RB.TabPane>
    );
  }

  renderAssignments() {
    return (
      <RB.TabbedArea animation={false}
        defaultActiveKey={this.get_oldest_ungraded_assignment(this.state.assignments)}>
          {this.state.assignments.map(this.renderAssignment.bind(this))}
      </RB.TabbedArea>
    );
  }

  render() {
    // Slight hack around asynchronous initialization
    if (!this.state || !this.state.selected_section) { // TODO: Simplify
      return null;
    } else if (this.state.selected_section.relation !== 'Section Leader') {
      return (<div>
          <SectionHeaderView sections={this.state.sections} auth_person={this.state.auth_person} view_person={this.state.view_person}/>
          {'Your current view is ' + this.state.selected_section.relation + ' (not an SL) and Kevin has been too lazy to implement that view. Give it some time homie.'} </div>);
    }
    console.log(this.state.selected_section);
    return (
        <div>
          <SectionHeaderView sections={this.state.sections} auth_person={this.state.auth_person} view_person={this.state.view_person}/>
          <RB.Grid>
            {this.renderAssignments()}
          </RB.Grid>
        </div>
    );
  }
}
`
