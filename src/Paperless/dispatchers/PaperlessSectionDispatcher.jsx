import {Dispatcher} from 'flux';
    
class PaperlessSectionDispatcher extends Dispatcher {
    handleAction(action) {
        this.dispatch({
            action
        });
    }
}

export default new PaperlessSectionDispatcher();
