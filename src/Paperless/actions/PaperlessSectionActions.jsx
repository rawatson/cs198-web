import PaperlessSectionDispatcher from '../dispatchers/PaperlessSectionDispatcher.jsx';
import PaperlessSectionActionTypes from '../constants/PaperlessSectionConstants.jsx';

export default {
  select_new_section: (section) => {
    PaperlessSectionDispatcher.handleAction({
      type: PaperlessSectionActionTypes.SELECT_NEW_SECTION,
      data: {section : section}
    });
  },
}
