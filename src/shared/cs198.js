'use strict';

// HTTP request library (uses promises)
let fetch = require('isomorphic-fetch');
let qs = require('query-string');

// https://stackoverflow.com/questions/1458724/how-to-set-unset-cookie-with-jquery
function readCookie(name) {
  var nameEQ = encodeURIComponent(name) + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
  }
  return null;
}

function getJWTPayload(token) {
  var payload = token.split('.')[1];
  var json = Base64.decode(payload);
  return JSON.parse(json);
}

let apiUrl = process.env.API_URL;
if (!apiUrl) {
  apiUrl = "http://localhost:3000";
}

function json(response) {
  return response.json()
}

function checkApiError(response) {
  if (response.error) {
    return Promise.reject(response);
  } else if (response.authenticationRequiredError) {
    var url = response.authenticationURL;
    url += '&redirect_url=' + encodeURIComponent(window.location);
    window.location.href = url;
    return Promise.reject('Needs authentication!');
  }
  return response;
}

function rp(path, arg_opts, method) {
  let opts = {method: method, credentials: 'include'};
  if (arg_opts && arg_opts.qs !== undefined) {
    path += '?' + qs.stringify(arg_opts.qs);
  }
  if (arg_opts && arg_opts.body !== undefined) {
    opts.headers = {'Content-type': 'application/json'};
    opts.body = JSON.stringify(arg_opts.body);
  }
  return fetch(apiUrl + path, opts)
    .then(json)
    .then(checkApiError);
}

function get(path, opts)  { return rp(path, opts, 'get'); };
function post(path, opts) { return rp(path, opts, 'post'); };
function put(path, opts)  { return rp(path, opts, 'put'); };
function del(path, opts)  { return rp(path, opts, 'delete'); };

module.exports = {
  Lair: {
    Shifts: {
      index: function(opts) {
        return get("/helper_shifts", { qs: opts });
      },
      // duration is in seconds
      create: function(opts) {
        return post("/helper_shifts", { body: opts });
      },
      swap: function(id_1, id_2) {
        return post("/helper_shifts/null/swap", {
          body: {
            helper_shift_id_1: id_1,
            helper_shift_id_2: id_2
          }
        });
      },
      update: function(id, opts) {
        return put("/helper_shifts/" + id, { body: opts });
      },
      destroy: function(id, isPerson) {
        if (isPerson === undefined || !isPerson) {
          return del("/helper_shifts/" + id);
        } else {
          return del("/helper_shifts/all_future_shifts", {body: {person_id: id}});
        }
      }
    },

    Helpers: {
      index: function(opts) {
        return get("/helper_checkins", { qs: opts });
      },
      checkin: function(person_id) {
        return post("/helper_checkins", {
          body: { person: person_id }
        });
      },
      checkout: function(helper_id) {
        return del("/helper_checkins/" + helper_id);
      },
    },

    Status: {
      find: function() {
        return get("/global_state/lair_signups_enabled");
      },
      update: function(values) {
        return put("/global_state/lair_signups_enabled", { body: values });
      }
    },
    Requests: {
      index: function(opts) {
        return get("/help_requests", { qs: opts });
      },
      
      show: function(sunet, opts) {
        return get(`/people/${sunet}/help_requests`, {qs: opts});
      },
      
      create: function(data) {
        return post("/help_requests", { body: data });
      },
    },
    Assignments: {
      index: function(opts) {
        return get("/helper_assignments", { qs: opts });
      },
      
      assign: function(help_request_id, helper_checkin_id) {
        var data = {helper_checkin_id: helper_checkin_id, help_request_id: help_request_id};
        return post("/helper_assignments" , { body: data });
      },
      
      reassign: function(helper_assignment_id, new_helper_checkin_id) {
        var data = {new_helper_checkin_id: new_helper_checkin_id};
        return put("/helper_assignments/" + helper_assignment_id, { body: data });
      },
     
      resolve: function(helper_assignment_id, data) {
        return del("/helper_assignments/" + helper_assignment_id, { body: data});
      }
    },
  },
  People: {
    find: function(id) {
      return get("/people/" + id);
    },
    sections: {
      index: function(sunetid, course_id, shallow) {
        console.log(course_id);
        if (shallow) {
          return get(`/people/${sunetid}/sections`, {qs: {course_id: course_id}});
        } else {
          return get(`/people/${sunetid}/sections`, {qs: {course_id: course_id, deep: true}});
        }
      }
    },
    getAuthenticatedPerson: function() {
      var token = readCookie('jwt');
      if (token) {
        // Don't verify the cookie on client side
        return Promise.resolve(getJWTPayload(token));
      } else {
        // Trigger a protected request to force login
        return get('/auth_tokens/1/protected_resources');
      }
    }
  },

  Assignments: {
    index: function(course_id) {
      console.log(course_id);
      return get(`/assignments`, {qs: {course_id: course_id}});
    }
  },

  AssignmentGrades: {
    index: function(course_id) {
      return get(`/assignment_grades`, {qs: {course_id: course_id}});
    }
  },

  Courses: {
    show: function(person_id, opts) {
      return get("/people/" + person_id + '/student_relations', { qs: opts });
    },
  },
  Staff: {
    show: function(person_id, opts) {
      return get("/people/" + person_id + '/staff_relations', { qs: opts });
    },
  },
  Auth: {
    login: function() {
      return get('/auth_tokens/1/protected_resources');
    }
  },
  Sandbox: {
    test: function() {
      return get('/auth_tokens/1/protected_resources');
    }
  }
};
