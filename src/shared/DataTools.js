'use strict';

var React            = require('react');

module.exports = {
  personToString : function(person) {
    return person.first_name + ' ' + person.last_name + ' (' + person.sunetid + ')'
  },

  sectionInformationToDiv : function(section) {
    var course = section.section_info.course;
    var course_name = course.department_code.toUpperCase() + course.course_number.toUpperCase();
    var quarter_name = course.quarter.quarter_name + ' ' + course.quarter.year;
    var relation = section.relation;
    return (<div className='text-center'>
              {course_name + ' ' + relation} <br/>
              {quarter_name}
            </div>);
  },

  sectionComparison : function(a, b) {
    var quantify_quarter_name = function(quarter_name) {
      if (quarter_name.toLowerCase() === "winter") {
        return 1;
      } else if (quarter_name.toLowerCase() === "spring") {
        return 2;
      } else if (quarter_name.toLowerCase() === "summer") {
        return 3;
      } else if (quarter_name.toLowerCase() === "autumn") {
        return 4;
      }
    }
    
    var a_course = a.section_info.course;
    var b_course = b.section_info.course;
    var a_year_as_int = parseInt(a_course.quarter.year);
    var b_year_as_int = parseInt(b_course.quarter.year);
    if (a_year_as_int - b_year_as_int !== 0) {
      return -(a_year_as_int - b_year_as_int);
    } else {
      var a_quarter_value = quantify_quarter_name(a_course.quarter.quarter_name);
      var b_quarter_value = quantify_quarter_name(b_course.quarter.quarter_name); 
      if (a_quarter_value - b_quarter_value !== 0) {
        return -(a_quarter_value - b_quarter_value);  
      } else {
        /* TODO: Expand on sorting better to include position and classes */
        return 0; 
      }
    }
  },
}

