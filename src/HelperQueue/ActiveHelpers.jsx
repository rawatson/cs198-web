'use strict';

let React       = require('react');
let _           = require('underscore');
let api         = require('../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'ActiveHelpers',
  handleSignIn: function(e) {
    e.preventDefault();

    let formItem = this.refs.sunetid.getDOMNode();
    let personId = formItem.value.toLowerCase();

    api.Lair.Helpers.checkin(personId).then(result => {
      formItem.value = "";
      let helper = result.data;

      let helpers = _.clone(this.props.helpers);
      helpers.push(helper);

      this.props.refresh(helpers);
    });
  },

  signOut: function(helper_id) {
    api.Lair.Helpers.checkout(helper_id).then(response => {
      this.props.refresh(this.props.helpers.filter(h => h.id !== helper_id));
    });
  },

  renderHelper: function(helper) {
    let elems = [];

    // Split each helper's info into 3 individual items
    let signOutButton = null;
    let helperName = <span className='active-helper-name'>
      {helper.person.first_name + " " + helper.person.last_name}
    </span>;
    let busyLabel = null;

    if (helper.currently_helping) {
      // If you're helping a student you get labeled as "busy"
      busyLabel = <span key='busy' className="active-helper-status">Busy</span>;
    } else {
      // If you're not helping, you can sign out
      let handler = e => {
        e.preventDefault();
        this.signOut(helper.id);
      };

      signOutButton = <a href="#" className='helper-sign-out list-btn' onClick={handler}>
        <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
      </a>;
    }

    return <li key={helper.person.sunetid}>
      {signOutButton}
      {helperName}
      {busyLabel}
    </li>;
  },

  render: function() {
    let helperList = <span>Loading helpers...</span>;

    if (this.props.helpers !== null) {
      helperList = <ul className='helper-list'>
        {this.props.helpers.map(this.renderHelper)}
      </ul>;
    }

    let signInForm = <form key='form' className="helper-sign-in" onSubmit={this.handleSignIn}>
      <input type="text" className="form-control" ref="sunetid"
        placeholder="SUNet ID" />
      <input className="btn btn-primary" type="submit" autoCapitalize="off" value="Sign in" />
    </form>

    return (
      <div className="active-helpers content-pane">
        {helperList}
        {signInForm}
      </div>
    );
  }
});
