'use strict';

var React   = require("react");
var moment  = require("moment");
var _       = require("underscore");

var api = require('../../shared/cs198.js');
var ShiftSelector = require("./ShiftSelector.jsx");

module.exports = (function(timezone) {
  return React.createClass({
    reset: function() {
      this.refs.toSwapSelector.reset();
      this.refs.replacementSelector.reset();
      this.setState({ toSwap: null, replacement: null });
    },

    getInitialState: function() {
      return {
        toSwap: null,
        replacement: null,
        successMessage: null,
        errors: null
      };
    },

    canSwap: function() {
      return (this.state.toSwap !== null) && (this.state.replacement !== null);
    },

    swap: function(e) {
      e.preventDefault();
      var req;
      if (this.state.replacementIsShift) {
        req = api.Lair.Shifts.swap(this.state.toSwap.id, this.state.replacement.id);
      } else {
        req = api.Lair.Shifts.update(this.state.toSwap.id,
                       { person_id: this.state.replacement.id });
      }

      req.then(result => {
        if (this.state.replacementIsShift) {
          this.setState({ successMessage: "Shifts swapped!" });
        } else {
          this.setState({ successMessage: "Shift reassigned to new helper!" });
        }
        this.reset();
      }, err => {
        err = err.error.data;
        var errList;

        switch (err.message) {
        case "Helper shift not found":
          alert("Something went very wrong! Please tell osdiab@cs.stanford.edu");
          break;
        case "Cannot swap shifts at the same time":
          this.setState({ errors: [err.message] });
          break;
        case "Validation error":
          this.setState({ errors: err.details.errors });
          break;
        case "Multi-record validation error":
          var shift_1_errors = [], shift_2_errors = [];
          if (err.details.records.shift_1) {
            shift_1_errors = err.details.records.shift_1.errors;
          }
          if (err.details.records.shift_2) {
            shift_2_errors = err.details.records.shift_2.errors;
          }

          errList = _.union(shift_1_errors, shift_2_errors);
          console.log(errList);

          this.setState({ errors: errList });
          break;
        }
      });
    },

    setShiftToSwap: function(shift, isShift) {
      if (isShift) {
        this.setState({ toSwap: shift });
      }
    },

    // If isShift == true, swap shifts; else, assign person to shift
    setSwapValue: function(replacement, isShift) {
      this.setState({ replacement: replacement, replacementIsShift: isShift });
    },

    render: function() {
      return <h1>TODO</h1>;
      var errors = this.state.errors;
      var elems = [];
      if (this.state.successMessage) {
        elems.push(<p className="success-message bg-success">
                {this.state.successMessage}
          </p>);
      }

      if (this.state.errors && this.state.errors.length > 0) {
        var errElems = _.map(this.state.errors, function(err) {
          return <li>{err}</li>;
        });
        elems.push(<ul className="errors bg-danger">{errElems}</ul>);
      }

      var leftText  = "Swap this shift...",
        rightText   = "... with this person or shift",
        buttonText  = "Please select a shift to swap first.";

      if (this.state.replacement !== null) {
        if (!this.state.replacementIsShift) {
          leftText = "Reassign this shift to...";
          rightText = "... this person:";
          buttonText = "Reassign!";
        } else {
          rightText = "... this shift:";

          if (this.state.toSwap !== null) {
            buttonText = "Swap!";
          }
        }
      }

      var selectors = <div className="row">
        <div className="col-sm-4">
          <p>{leftText}</p>
          <ShiftSelector ref="toSwapSelector" timezone={timezone}
            callback={this.setShiftToSwap} />
        </div>
        <div className="col-sm-4">
          <p>{rightText}</p>
          <ShiftSelector ref="replacementSelector"
            allowEmptyShifts={true} timezone={timezone}
            callback={this.setSwapValue} />
        </div>
      </div>;

      elems.push(selectors);
      elems.push(<input type="submit" className="btn btn-primary" onClick={this.swap}
        disabled={!this.canSwap()} value={buttonText} />);

      return (
        <div className="swap-shifts">
          <header className="row title">
            <h1>Swap Shifts</h1>
          </header>
          <div className="row">
            <div className="col-xs-12">
              {elems}
            </div>
          </div>
        </div>
      );
    }
  });
});
