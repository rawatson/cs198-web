'use strict';

var React   = require("react");
var moment  = require("moment");
var _       = require("underscore");

var api = require('../../shared/cs198.js');
var ShiftSelector = require("./ShiftSelector.jsx");
var PersonSelector = require("./PersonSelector.jsx");

module.exports = (function(timezone) {
  return React.createClass({
    mixins: [React.addons.LinkedStateMixin],

    displayName: 'CoverShiftView',

    reset: function() {
      this.refs.toSwapSelector.reset();
      this.refs.newPersonSelector.reset();
      this.setState({ toSwap: null, newPerson: null });
    },

    getInitialState: function() {
      return {
        toSwap: null,
        newPerson: null,
        message: null,
        error: false
      };
    },

    canSwap: function() {
      return this.state.toSwap && this.state.newPerson;
    },

    swap: function(e) {
      e.preventDefault();
      var toSwap = this.state.toSwap, newPerson = this.state.newPerson;
      var opts = {person_id: newPerson.id, regular_shift: false};

      api.Lair.Shifts.update(toSwap.id, opts)
      .then(result => {
        console.log(result);
        var message = `Shift reassigned to ${newPerson.first_name} ${newPerson.last_name}`;
        this.setState({ message: message, error: false });
        this.reset();
      }, err => {
        this.setState({ message: err.error, error: true});
      });
    },

    setShiftToSwap: function(toSwap) {
      this.setState({ toSwap: toSwap });
    },

    setNewPerson: function(newPerson) {
      this.setState({ newPerson: newPerson });
    },


    render: function() {
      var message = null;
      if (this.state.message) {
        if (this.state.error) {
          message = <p className="errors bg-danger">
            {this.state.message}
          </p>
        } else {
          message = <p className="success-message bg-success">
            {this.state.message}
          </p>
        }
      }

      var selectors = <div className="row">
        <div className="col-xs-6 col-xs-offset-3">
          {message}
        </div>
        <div className="row" style={{marginBottom:'0px'}}>
          <div className="col-xs-4 col-xs-offset-2">
            <p>Enter the SUNetID and select the shift which is being <b>given away</b>.</p>
          </div>
          <div className="col-xs-4">
            <p>Enter the SUNetID of the person who will be <b>taking over</b> the shift.</p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 col-xs-offset-2">
            <ShiftSelector ref="toSwapSelector" timezone={timezone}
              callback={this.setShiftToSwap} />
          </div>
          <div className="col-xs-4">
          <PersonSelector ref="newPersonSelector" callback={this.setNewPerson}/>
          </div>
        </div>
      </div>;

      return (
        <div className="swap-shifts">
          <header className="row title">
            <h1>Swap Shifts</h1>
          </header>
          <div className="container">
            {selectors}
            <div className="col-xs-4 col-xs-offset-4">
              <input type="submit" className="btn btn-primary btn-lg" onClick={this.swap}
                disabled={!this.canSwap()} value="Reassign shift!" />
            </div>
          </div>
        </div>
      );
    }
  });
});
