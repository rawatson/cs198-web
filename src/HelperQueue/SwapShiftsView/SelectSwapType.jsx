'use strict';

var React   = require("react");
var moment  = require("moment");
var _       = require("underscore");

var api = require('../../shared/cs198.js');

module.exports = (function(timezone) {
  return React.createClass({
    displayName: 'SelectSwapType',

    render: function() {
      return (
        <div className="swap-shifts">
          <header className="row title">
            <h1>Swap Shifts</h1>
          </header>
          <div className="center-block container-fluid">
            <div className="col-sm-6">
              <a href="swap/one_for_one">
                <button className="btn btn-lg btn-primary center-block">
                  Swap one shift
                </button>
              </a>
              <p>
                Trade one of your LaIR shifts for someone else's.  
              </p>
            </div>

            <div className="col-sm-6">
              <a href="swap/cover">
                <button className="btn btn-lg btn-primary center-block">
                  Cover someone's shift
                </button>
              </a>
              <p>
                Take over someone else's shift without giving them one of your own
              </p>
            </div>

          </div>
          <div className="center-block container-fluid">
            <div className="col-sm-12">
              <p style={{textAlign: 'center'}}>If you need to make a permanent swap, <a href="mailto:cs198@cs.stanford.edu">email the coordinators</a>.</p>
            </div>
          </div>
        </div>
      );
    }
  });
});
