'use strict';

var React   = require("react");
var moment  = require("moment");
var _       = require("underscore");

var api = require('../../shared/cs198.js');
var ShiftSelector = require("./ShiftSelector.jsx");

module.exports = (function(timezone) {
  return React.createClass({
    displayName: 'SwapShiftsView',

    reset: function() {
      this.refs.toSwapSelector.reset();
      this.refs.replacementSelector.reset();
      this.setState({ toSwap: null, replacement: null });
    },

    getInitialState: function() {
      return {
        toSwap: null,
        replacement: null,
        message: null,
        error: false
      };
    },

    canSwap: function() {
      return this.state.toSwap && this.state.replacement;
    },

    swap: function(e) {
      e.preventDefault();
      api.Lair.Shifts.swap(this.state.toSwap.id, this.state.replacement.id)
      .then(result => {
        this.setState({ message: "Shifts swapped!", error: false });
        this.reset();
      }, err => {
        this.setState({ error: true, message: err.error });
      });
    },

    setShiftToSwap: function(toSwap) {
      this.setState({ toSwap: toSwap });
    },

    setReplacement: function(replacement) {
      this.setState({ replacement: replacement});
    },

    render: function() {
      var message = null;
      if (this.state.message) {
        if (this.state.error) {
          message = <p className="errors bg-danger">
            {this.state.message}
          </p>
        } else {
          message = <p className="success-message bg-success">
            {this.state.message}
          </p>
        }
      }

      var selectors = <div className="row">
        <div className="col-xs-6 col-xs-offset-3">
          {message}
        </div>
        <div className="row" style={{marginBottom:'0px'}}>
          <div className="col-xs-4 col-xs-offset-2">
            <p>Enter your SUNetID and select the shift you'd like to swap <b>out of</b>.</p>
          </div>
          <div className="col-xs-4">
            <p>Enter the SUNetID of the person you're swapping with and select the shift you want to swap <b>into</b>.</p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-4 col-xs-offset-2">
            <ShiftSelector ref="toSwapSelector" timezone={timezone}
              callback={this.setShiftToSwap} />
          </div>
          <div className="col-xs-4">
            <ShiftSelector ref="replacementSelector"
              timezone={timezone}
              callback={this.setReplacement} />
          </div>
        </div>
      </div>;

      return (
        <div className="swap-shifts">
          <header className="row title">
            <h1>Swap Shifts</h1>
          </header>
          <div className="container">
            {selectors}
            <div className="col-xs-4 col-xs-offset-4">
              <input type="submit" className="btn btn-primary btn-lg" onClick={this.swap}
                disabled={!this.canSwap()} value="Complete swap!" />
            </div>
          </div>
        </div>
      );
    }
  });
});
