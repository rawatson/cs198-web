'use strict';

var React   = require("react");
var moment  = require("moment-timezone");

var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'ShiftSelector',

  reset: function() {
    this.refs.person_id.getDOMNode().value = '';
    this.setState(this.getInitialState());
  },

  handleShiftClick: function(shift) {
    return () => {
      this.props.callback(shift, true);
    };
  },

  handleSubmit: function(e) {
    e.preventDefault();

    // gets person
    var personId = this.refs.person_id.getDOMNode().value.toLowerCase();
    api.People.find(personId).then(result => {
      var person = result.data;
      if (person.staff_status !== "Active") {
        // error
        alert("Person not staff!");
        return;
      }

      api.Lair.Shifts.index({person_id: person.id, after: moment().format()}).then( result => {
        var shifts = result.data;
        if ((this.props.allowEmptyShifts !== true) &&
          shifts.length === 0) {
          // error
          alert("Person has no shifts to swap!");
          return;
        }

        this.setState({
          person: person,
          shifts: shifts
        });
      });
    }, function(err) {
      if (err.statusCode === 404) {
        alert("Person '" + personId + "' not found! SUNet IDs, suids, " +
            "and first + last names are accepted. Please try again.");
      }
    });
  },

  personInput: function() {
    return [
      <input type="text" className="form-control same-line" ref="person_id" size="9"
        placeholder="SUNetID" />,
      <input type="submit" className="btn btn-primary" value="List shifts" />
    ];
  },

  shiftInput: function() {
    var shiftRadioBtns = this.state.shifts.map(shift => {
      var time_text = moment(shift.start_time).tz(this.props.timezone).format(
        "dddd, MMM D, h:mm A");
      return <li><input type="radio" name={this.state.formName}
              value={shift.id} onClick={this.handleShiftClick(shift)}>
        {time_text}
      </input></li>;
    });
    return <ul className="shift-selector-shifts">{shiftRadioBtns}</ul>;
  },

  getInitialState: function() {
    return {
      formName: "shift-selector-" + Math.random(),
      shifts: null,
      person: null
    };
  },

  render: function() {
    var elems = [this.personInput()];

    if (this.state.person !== null) {
      var person = this.state.person;
      elems.push(
        <p style={{fontWeight: 'bold', paddingTop:'14px', marginBottom:'6px'}}>
          {person.first_name + " " + person.last_name}
        </p>);
      elems.push(this.shiftInput());
    }

    return (
      <div className="shift-selector"><form onSubmit={this.handleSubmit}>
        {elems}
      </form></div>
    );
  }
});
