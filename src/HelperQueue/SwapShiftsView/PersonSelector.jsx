'use strict';

var React   = require("react");
var moment  = require("moment-timezone");

var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'PersonSelector',

  reset: function() {
    this.refs.person_id.getDOMNode().value = '';
    this.setState(this.getInitialState());
  },

  handleSubmit: function(e) {
    e.preventDefault();

    // gets person
    var personId = this.refs.person_id.getDOMNode().value.toLowerCase();
    api.People.find(personId).then(result => {
      var person = result.data;
      if (person.staff_status !== "Active") {
        // error
        alert("Person not staff!");
        return;
      }

      this.props.callback(person);
      this.setState({person: person});
    }, function(err) {
      if (err.statusCode === 404) {
        alert("Person '" + personId + "' not found! SUNet IDs, suids, " +
            "and first + last names are accepted. Please try again.");
      }
    });
  },

  personInput: function() {
    return [
      <input type="text" className="form-control same-line" ref="person_id" size="9"
        placeholder="SUNetID" />,
      <input type="submit" className="btn btn-primary" value="Get person" />
    ];
  },


  getInitialState: function() {
    return {
      person: null
    };
  },

  render: function() {
    var elems = [this.personInput()];

    if (this.state.person !== null) {
      var person = this.state.person;
      elems.push(
        <p style={{fontWeight: 'bold', paddingTop:'14px', marginBottom:'6px'}}>
          {person.first_name + " " + person.last_name}
        </p>);
    }

    return (
      <div className="shift-selector"><form onSubmit={this.handleSubmit}>
        {elems}
      </form></div>
    );
  }
});
