'use strict';

var React       = require('react');
var moment      = require('moment');

var SignupForm    = require('./SignupForm.jsx');
var ActiveHelpers   = require('../ActiveHelpers.jsx');
var api = require('../../shared/cs198.js');

module.exports =  React.createClass({
  displayName: 'SignupView',

  PREV_REQUEST_TIMEOUT: 15, // minutes to allow students to make a new request

  REFRESH_RATE: 10000, // milliseconds to refresh

  getInitialState: function() {
    return {
      student: null,
      queueStatus: {lair_signups_enabled: null}
    };
  },

  refreshState: function() {
    //this.refreshActiveHelpers();
    this.refreshQueueStatus();
    this.refreshRecentRequests();
  },

  refreshRecentRequests: function() {
    api.Lair.Requests.index({
      open: false,
      since: moment().subtract(this.PREV_REQUEST_TIMEOUT, "minutes").utc().format()
    }).then( result => {
      this.setState({ recentRequests: result.data });
    });
  },

  refreshActiveHelpers: function(data) {
    if (data) return this.setState({ helpers: data });

    api.Lair.Helpers.index().then(result => {
      this.setState({ helpers: result.data });
    });
  },

  refreshQueueStatus: function(data) {
    if (data) return this.setState({ queueStatus: data });

    api.Lair.Status.find().then(result => {
      this.setState({queueStatus: result.data});
    });
  },

  resetForm: function() {
    if (typeof this.resetTimer !== 'undefined') {
      clearTimeout(this.resetTimer);
      this.resetTimer = undefined;
    }
    this.setState({student: null});
  },

  onHelpRequest: function(student, helpRequest) {
    this.setState({student: student, helpRequest: helpRequest});
  },

  positionMessage: function(pos) {
    switch (pos) {
    case 0:
      return "You are next to receive help!";
    case 1:
      return "There is one person ahead of you.";
    default:
      return "There are " + pos + " people ahead of you.";
    }
  },

  FORM_RESET_TIMEOUT: 10000,

  renderWelcomeState: function() {
    var elems;
    if (this.state.queueStatus.lair_signups_enabled === true) {
      //var clair = (moment().hours() >= 20 && moment().hours() <= 21) ? 
      //  (<div>
      //     <p>Looking for conceptual help? Sign up for the CLaIR!<br/>Open Sunday-Thursday, 8-10PM.</p>
      //     <iframe src="https://docs.google.com/forms/d/1uX8H-54KvFpUkBPz-lZS-ce921T2l_dj0UVrBVdGj64/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
      //   </div>) : null;
      elems = (<div>
                <SignupForm submitCallback={this.onHelpRequest}
                            recentRequests={this.state.recentRequests}/>
      //          {clair}
              </div>);
    } else if (this.state.queueStatus.lair_signups_enabled === null) {
      elems = <p>Loading...</p>;
    } else {
      elems = <p>Sorry, signups have been disabled now. Have a good day!</p>;
    }
    return [
      <header key='welcomeMessage' className="row title">
        <div className="col-lg-12">
          <h1>Welcome to the LaIR</h1>
        </div>
      </header>,
      <div key='signupOrDisabledMessage' className="row">
        <div className="col-lg-12">
          {elems}
        </div>
      </div>
    ];
  },

  renderSubmittedState: function() {
    // reset the form after delay
    // TODO: make the timeout visible
    if (typeof this.resetTimer === 'undefined') {
      this.resetTimer = setTimeout(this.resetForm, this.FORM_RESET_TIMEOUT);
    }

    var queuePositionMessage;
    if (this.state.helpRequest) {
      var message = this.positionMessage(this.state.recentRequests.length);
      queuePositionMessage = <p>{message}</p>;
    }

    return ([
      <header key='nameConfirm' className="row title">
        <h1>{this.state.student.first_name}, we have your help request!</h1>
      </header>,
      <div key='queuePositionInfo' className="row">
        {queuePositionMessage}
        <p>We will do our best to get to you as quickly as possible.</p>,
        <button className="btn" onClick={this.resetForm}>Return to request help</button>
      </div>
    ]);
  },

  componentDidMount: function() {
    this.refreshInterval = setInterval(this.refreshState, this.REFRESH_RATE);
    this.refreshState();
  },

  componentWillUnmount: function() {
    clearInterval(this.refreshInterval);
  },

  render: function() {
    var content;
    if (this.state.student === null) {
      content = this.renderWelcomeState();
    } else {
      content = this.renderSubmittedState();
    }

    // TODO: re-enable when staff pics are ready
    //if (this.state.queueStatus.signups_enabled) {
      //content.push(<h2>Current LaIR helpers</h2>);
      //content.push(<ActiveHelpers helpers={this.state.helpers}
        //staff={false}
        //api={api} />);
    //}
    return (
      <div className="signup-page">
        {content}
      </div>
    );
  }
});
