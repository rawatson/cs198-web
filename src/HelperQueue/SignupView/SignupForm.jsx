'use strict';

var React     = require('react');
var _       = require('underscore');
var classNames  = require('classnames');
var moment = require('moment');

var FormErrors  = require('../../shared/FormErrors.jsx');
var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'SignupForm',

  handleSubmit: function(e) {
    e.preventDefault();
    if (this.state.student === null) {
      this.handleStudentSearch();
    } else {
      this.handleRequestSubmission();
    }
  },

  handleStudentSearch: function() {
    /* 15 minute check */
    var TIME_TO_WAIT = 15;
    var MAX_LAIR_HISTORY = 360;

    var student, courses;

    var signup_invalid = (help_requests, ttw) => {
      for (var i = 0; i < help_requests.length; i++) {
        var helper_assignment = help_requests[i].helper_assignment;
        if (helper_assignment === null || helper_assignment === undefined) {
          // Send them to the "signup success" screen if already in the queue
          this.props.submitCallback(student, help_requests[i]);
          return true;
        }
        if (helper_assignment.close_time === null || helper_assignment.close_time === undefined) {
          throw "Our records show that you're being helped by someone right now.  "
              + "Please find an SL to figure out what's going on";
        }
        var threshold = moment().subtract(TIME_TO_WAIT, 'minutes');
        var close_time = moment(helper_assignment.close_time);
        if (close_time.isAfter(threshold)) {
          throw `Sorry, but you need to wait at least ${TIME_TO_WAIT} minutes
                 since your previous request has ended before you can ask for 
                 more help. Come back soon!`;
        }
      }
      return false;
    };
  
    var sunetId = this.refs.sunetid.getDOMNode().value.toLowerCase();

    // Look up the SUNetID they typed in
    api.People.find(sunetId)

    .catch(error => {
      throw `We can't find anyone with the name '${sunetId}'.  `;
          + `This is your Stanford email without @stanford.edu on the end`;
    })

    // Retrieve what classes they're taking
    .then(result => {
      student = result.data;
      return api.Courses.show(student.id, {student: true});
    })
    
    .catch(error => {
      if (!('error' in error)) throw error; // rethrow if not an API error
      throw "We ran into an error while fetching your course records.  "
          + "Please contact an SL"
    })

    // Check that they're taking a LaIR-able class and load their current requests
    .then (course_response => {
      courses = course_response.data;
      if (courses.length === 0) {
        throw "It doesn't look like you're taking any courses we can help " +
          "you with at the LaIR. Please come back when you are!";
      }

      return api.Lair.Requests.show(sunetId, {
        desired_history: MAX_LAIR_HISTORY
      })
    }) 

    .catch(error => {
      if (!('error' in error)) throw error; // rethrow if not an API error
      throw `An error happened when looking up your help requests. `
            + `Please ask a section leader for help!`;
    })

    // Check they aren't currently in the queue, move to next screen if not
    .then(result => {
      var help_requests = result.data;
      if (signup_invalid(help_requests, TIME_TO_WAIT)) {
        return;
      }

      this.clearErrors();
      this.setState({student: student, courses: courses});
    })
    
    .catch(error => {
      console.log(error);
      this.setState({errors: [error]});
    })
  },

  handleRequestSubmission: function() {
    var course_id;
    var data = {
      problem_description: this.refs.description.getDOMNode().value,
      location: this.refs.location.getDOMNode().value,
      person_id: this.state.student.id,
      course_id: this.refs.course.getDOMNode().value
    };

    api.Lair.Requests.create(data)
    .then(result => {
      var request = result.data;
      return this.props.submitCallback(this.state.student, request);
    }, function(err) {
      // TODO: handle error message. Not sure why I can't receive the specific message.
      alert("Please make sure your form has all required parts filled.");
      console.log(`Status code: ${err.statusCode}`);
      console.log(err.body);
      console.log(err);
      console.log(JSON.stringify(err));
    });
  },

  handleCancel: function(e) {
    this.setState({student: null, courses: null});
  },

  clearErrors: function() {
    this.setState({errors: []});
  },

  componentDidMount: function() {
    var clearErrors = this.clearErrors;
  },

  getInitialState: function() {
    return {errors: [], student: null, courses: null};
  },

  renderStudentUnchosen: function() {
    return [
      <p key='0'>Enter your SUNetID to request LaIR help or view the status of your request.</p>,
      <div key='1'>
        <input className="form-control signup-form-user" type="text" ref="sunetid"
          placeholder="SUNet ID (i.e. YourID@stanford.edu)" onInput={this.clearErrors} />
        <input className="btn btn-primary" type="submit" value="Request help" />
      </div>
    ];
  },

  renderCourseFormElem: function(student) {
    var course;
    if (this.state.courses.length > 1) {
      var options = this.state.courses.map(student_relation => {
        var c = student_relation.course;
        return <option key={c.id} value={c.id}>{c.department_code + c.course_number}</option>;
      });
      return <select ref="course">{options}</select>;
    } else {
      var c = this.state.courses[0].course;
      console.log(c);
      return (
        <span className="course-final">
          {c.department_code + c.course_number}
          <input type="hidden" ref="course" value={c.id} />
        </span>
      );
    }
  },

  renderStudentChosen: function() {
    var course = this.renderCourseFormElem();
    return (
      <div className="signup-form-contents">
        <button type="button" className="close-btn btn" onClick={this.handleCancel}>
          Nevermind</button>
        <ul>
          <li>
            <span className="inline-label">SUNet ID:</span>
            <span className="user-final">
              {this.state.student.sunetid}
            </span>
          </li>
          <li>
            <span className="inline-label">Course:</span>
            {course}
          </li>
          <li>
            <textarea className="form-control" rows="3" ref="description"
              placeholder="Describe your problem... (in detail, please!)" />
          </li>
          <li>
            <input className="form-control student-location" type="text" ref="location"
              placeholder="Your location" />
            <input className="btn btn-primary" type="submit" value="Request help" />
          </li>
          <li>
          </li>
        </ul>
        //<img width="100%" src="/img/LairMap.png" />
      </div>
    );
  },

  render: function() {
    var errors, formElems;
    var className = classNames('row', 'signup-form', {
      'form-inline': this.state.student === null
    });

    if (this.state.errors.length > 0) {
      errors =
        <div className="col-xs-12 col-sm-6 col-sm-offset-3 bg-danger pane">
          <FormErrors errors={this.state.errors} />
        </div>;
    }

    if (this.state.student === null) {
      formElems = this.renderStudentUnchosen();
    } else {
      formElems = this.renderStudentChosen();
    }
    return (
      <form className={className} onSubmit={this.handleSubmit}>
        <div className="col-sm-6 col-sm-offset-3 pane">{formElems}</div>
        {errors}
      </form>
    );

  }
});
