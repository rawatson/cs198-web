'use strict';

var React   = require('react');
var moment  = require('moment');
var classNames  = require('classnames');

var AssignHelperForm = require("./AssignHelperForm.jsx");
var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'HelpRequest',

  resolveRequest: function(reason) {
    var helper_assignment_id = this.props.request.helper_assignment.id;
    api.Lair.Assignments.resolve(helper_assignment_id, {reason: reason}).then(() => {
      this.props.refresh();
    });
  },

  assignRequest: function(verb, helper_id) {
    if (typeof helper_id === "string") helper_id = parseInt(helper_id);
  
    if (verb === 'reassign' || verb === 'reopen') {
      var helper_assignment_id = this.props.request.helper_assignment.id;
      api.Lair.Assignments.reassign(helper_assignment_id, helper_id).then(() => {
        this.props.refresh();
      });
    } else {
      api.Lair.Assignments.assign(this.props.request.id, helper_id).then(() => {
        this.props.refresh();
      }).catch((err) => {
        window.alert(err.error);
      });
    }
  },

  availableHelpers: function() {
    return this.props.helpers.filter(h => !h.currently_helping);
  },

  renderAssignmentButtons: function(request) {
    if (request.helper_assignment) {
      if (request.helper_assignment.close_time === null) {
        var resolveRequestHandler = function(reason, e) {
          e.preventDefault();
          this.resolveRequest("Reason is: resolved");
        };

        return [
          <button key="mark-student-left" className="btn btn-default"
            onClick={resolveRequestHandler.bind(this, "left")}>Student Left</button>,
          <button key="mark-request-resolved" className="btn btn-primary"
            onClick={resolveRequestHandler.bind(this, "resolved")}>Mark Resolved</button>,
          <AssignHelperForm
            key="reassign-live-request"
            availableHelpers={this.availableHelpers()}
            callback={this.assignRequest.bind(this, 'reassign')}
            prompt="Reassign to..." verb="reassign" />
        ];
      } else {
        var callback = this.assignRequest.bind(this, 'reopen');
        return <AssignHelperForm
          availableHelpers={this.availableHelpers()}
          callback={callback}
          prompt="Reopen and assign to..." verb="reassign" />;
      }
    } else {
      var callback = this.assignRequest.bind(this, 'assign');
      return <AssignHelperForm
        availableHelpers={this.availableHelpers()}
        callback={callback}
        prompt="Assign to..." verb="assign" />;
    }
    return assignElems;
  },

  renderHelperInfo: function(request) {
    var helper_assignment = request.helper_assignment
    if (helper_assignment && helper_assignment.close_time === null) {
      var helper = helper_assignment.helper_checkin.person;
      return <span className="request-assignment">
        {helper.first_name + " " + helper.last_name + " is helping"}
      </span>;
    }
  },

  renderRequestSummary: function(request) {
    var student = request.student_relation.person;
    var course = request.student_relation.course;
    var courseTitle = course.department_code + course.course_number;
    return <div className="row">
      <div className="row-items col-lg-4 col-md-2">
        <span className="request-student">
          {student.first_name + " " + student.last_name}
        </span>
        <span className="request-course">
          {courseTitle}
        </span>
      </div>
      <div className="row-items assign-btns col-md-10 col-lg-8">
        {this.renderAssignmentButtons(request)}
      </div>
    </div>
  },

  render: function() {
    var request = this.props.request;

    var className = classNames('clearfix', 'help-request', {
      'bg-success': !!this.props.highlight, 'bg-warning': !!this.props.progress });

    return (
      <div className={className}>
        {this.renderRequestSummary(request)}
        <div className="request-description">
          <p>{request.problem_description}</p>
        </div>
        <div>
          <span className="request-location">
            {"Location: " + request.location}
          </span>
          <span className="request-timestamp">
            {moment(request.request_time).format("h:mm A")}
          </span>
        </div>
        {this.renderHelperInfo(request)}
      </div>
    );
  }
});
