'use strict';

var React       = require('react');
var _           = require('underscore');
var classNames  = require('classnames');

var HelpRequest = require('./HelpRequest.jsx');

module.exports = React.createClass({
  displayName: 'HelpRequests',

  getInitialState: function() {
    return {
      unassignedExpanded: false,
      closedExpanded: false
    };
  },

  toggleExpandMore: function(type) {
    return e => {
      e.preventDefault();

      switch(type) {
      case "unassigned":
        return this.setState({unassignedExpanded: !this.state.unassignedExpanded});
      case "closed":
        return this.setState({closedExpanded: !this.state.closedExpanded});
      }
    };
  },

  renderRequestList: function(requests, opts) {
    if (typeof opts === 'undefined') opts = {};

    var renderOneReq = req => {
      var progress = opts.inProgress ? true : false;
      var elem = <HelpRequest
        helpers={this.props.helpers}
        request={req}
        refresh={this.props.refresh}
        progress={progress}/>;

      return <li key={req.id}>{elem}</li>;
    }

    return (
      <ul>
        {requests.map(renderOneReq)}
      </ul>
    );
  },

  renderAssigned: function() {
    var elem;
    if (_.isEmpty(this.props.requests.assigned)) {
      elem = <p>No assigned requests.</p>;
    } else {
      elem = this.renderRequestList(this.props.requests.assigned, {inProgress: true});
    }

    return (
      <div className="request-list requests-assigned">
        <h4>In progress</h4>
        {elem}
      </div>
    );
  },

  renderUnassigned: function() {
    var elems;
    if (_.isEmpty(this.props.requests.unassigned)) {
      elems = <p>No unassigned requests!</p>;
    } else {
      var moreClass = classNames('requests-more',
                     { hidden: !this.state.unassignedExpanded }),
        toggleText = this.state.unassignedExpanded ? 'Fewer requests' :
                               `More requests (${this.props.requests.unassigned.length - 1})...`;

      elems = [
        (<HelpRequest key="first-help-request" helpers={this.props.helpers}
                request={_.first(this.props.requests.unassigned)}
                refresh={this.props.refresh}
                highlight={true}/>)
      ];
      if (!_.isEmpty(_.tail(this.props.requests.unassigned))) {
        elems = elems.concat([
          (<a key="toggle-unassigned" href=""
            onClick={this.toggleExpandMore("unassigned").bind(this)}>{toggleText}</a>),
          (<div key="more-unassigned" className={moreClass}>
            {this.renderRequestList(_.tail(this.props.requests.unassigned))}
          </div>)
        ]);
      }
    }

    return (
      <div className="request-list requests-unassigned">
        <h4>Up next:</h4>
        {elems}
      </div>
    );
  },

  renderClosed: function() {
    var elems;
    if (_.isEmpty(this.props.requests.closed_recently)) {
      elems = <p>No recently closed requests.</p>;
    } else {
      var requestList = this.renderRequestList(this.props.requests.closed_recently),
        toggleText = this.state.closedExpanded ? 'Collapse' : 'Expand...',
        moreClass = classNames('requests-more', {hidden: !this.state.closedExpanded});

      elems = [
        <a key="closed-toggle" href="" onClick={this.toggleExpandMore("closed").bind(this)}>{toggleText}</a>,
        <div key="closed-requests" className={moreClass}>
          {requestList}
        </div>
      ];
    }

    return (
      <div className="request-list requests-closed">
        <h4>Recently closed</h4>
        {elems}
      </div>
    );
  },

  render: function() {
    var requestsElem;

    if (this.props.requests === null) {
      requestsElem = <span>Loading...</span>;
    } else {
      requestsElem = (
        <div className="help-requests-container">
          {this.renderUnassigned()}
          {this.renderAssigned()}
          {this.renderClosed()}
        </div>
      );
    }

    return (
      <div className="content-pane help-requests">
        {requestsElem}
      </div>
    );
  }
});
