'use strict';

var React = require('react');

var api   = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'QueueStatus',

  enableTooltip: function() {
    if (this.refs.enableLink) {
      $(this.refs.enableLink.getDOMNode()).tooltip();
    }
  },

  fixTooltip: function() {
    if (this.refs.enableLink) {
      var link = this.refs.enableLink.getDOMNode();
      $(link).tooltip('fixTitle');
    }
  },

  componentDidMount: function() {
    this.enableTooltip();
  },

  componentDidUpdate: function() {
    this.fixTooltip();
  },

  enabledMessage: function(enabled) {
    if (enabled === null) {
      return <span>Loading queue status...</span>;
    }
    var actionText = (enabled ? "Disable" : "Enable");
    var link = (
      <a href="#" ref="enableLink"
        onClick={this.handleToggleEnabled}
        data-toggle="tooltip"
        data-placement="top"
        title={actionText}>
        {(enabled ? "enabled" : "disabled")}
      </a>);

    return <span>Signups {link}</span>;
  },

  numRequestsMessage: function(numRequests) {
    if (numRequests === 1) {
      return "1 unclaimed request";
    }
    return (numRequests === null ? "?" : numRequests) + " unclaimed requests";
  },

  waitTimeMessage: function(waitTime) {
    return "Wait time: " + (waitTime === null ? "?" : waitTime);
  },

  handleToggleEnabled: function(e) {
    e.preventDefault();
    $(this.refs.enableLink.getDOMNode()).tooltip('hide');

    if (this.props.enabled === null) return;
    var enabled = !this.props.enabled;
    api.Lair.Status.update({lair_signups_enabled: enabled}).then(result => {
      this.props.refresh(result.data.lair_signups_enabled);
    });
  },

  render: function() {
    // required variables for render
    var numRequestsMessage = this.numRequestsMessage(this.props.numRequests);
    var waitTimeMessage = this.waitTimeMessage(this.props.waitTime);
    var openStatusMessage = this.enabledMessage(this.props.enabled);
    var toggleEnabled = this.props.enabled !== null;

    return (
      <div className="content-pane queue-status">
        <ul>
          <li>{openStatusMessage}</li>
          <li><span>{numRequestsMessage}</span></li>
          <li><span>{waitTimeMessage}</span></li>
        </ul>
      </div>
    );
  }
});
