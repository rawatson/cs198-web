'use strict';

var React   = require('react');
var moment  = require("moment");
var _       = require("underscore");

var QueueStatus = require('./QueueStatus.jsx');
var ActiveHelpers = require('../ActiveHelpers.jsx');
var HelpRequests = require('./HelpRequests.jsx');
var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  displayName: 'HelperView',

  REACTIVATE_LEFT_TIMEOUT: 15, // minutes to reactivate reqs from students who left

  REFRESH_RATE: 5000,      // milliseconds to refresh

  refreshState: function() {
    this.refreshActiveHelpers();
    this.refreshHelpRequests();
    this.refreshQueueStatus();
  },

  refreshActiveHelpers: function(data) {
    if (data) return this.setState({ helpers: data });

    api.Lair.Helpers.index().then(result => {
      this.setState({ helpers: result.data });
    });
  },

  refreshHelpRequests: function(data) {
    if (data) return this.setState({ requests: data });

    Promise.all([
      api.Lair.Requests.index(),
      api.Lair.Assignments.index({resolved: false}),
      api.Lair.Assignments.index({
        resolved: true,
        since: moment().subtract(this.REACTIVATE_LEFT_TIMEOUT, "minutes").utc().format()
      })
    ]).then(results => {
      var unassigned = results[0].data;
      var assigned = results[1].data;
      var resolved = results[2].data;
      var requests = {
        unassigned: unassigned.sort((a,b) => {
          if (a.request_time < b.request_time)
            return -1;
          else if (a.request_time > b.request_time)
            return 1;
          else return 0;
        }),
        closed_recently: resolved,
        assigned: assigned
      };

      this.setState({requests: requests});
    });

  },
  
  refreshQueueStatus: function(data) {
    if (data) return this.setState({ queueStatus: data });
    api.Lair.Status.find().then(result => {
      this.setState({ queueStatus: result.data });
    });
  },

  getInitialState: function() {
    return {
      queueStatus: null,
      requests: null,
      helpers: null
    };
  },

  componentDidMount: function() {
    this.refreshInterval = setInterval(this.refreshState, this.REFRESH_RATE);
    this.refreshState();
  },

  componentWillUnmount: function() {
    clearInterval(this.refreshInterval);
  },

  render: function() {
    var numRequests = null;
    if (this.state.requests !== null) {
      numRequests = this.state.requests.unassigned.length;
    }
    var enabled = null;
    if (this.state.queueStatus !== null) {
      enabled = this.state.queueStatus.lair_signups_enabled;
    }
    var waitTime = null;
    if (this.state.requests !== null) {
      if (this.state.requests.unassigned.length === 0) {
        waitTime = "None!";
      } else {
        var nextToHelp = _.first(this.state.requests.unassigned);
        var earliestRequestTime = moment(nextToHelp.request_time);
        waitTime = moment().toDate() - earliestRequestTime.toDate();
        waitTime = Math.ceil((waitTime / 1000) / 60);
        waitTime = waitTime + " minutes";
      }
    }

    return (
      <div className="helper-queue">
        <header className="row title">
          <h1>Helper Queue</h1>
        </header>
        <div className="row">
          <div className="col-sm-8">
            <div className="row requests-panel">
              <h2>Help requests</h2>
              <HelpRequests helpers={this.state.helpers}
                requests={this.state.requests}
                refresh={() => {
                  this.refreshHelpRequests();
                  this.refreshActiveHelpers();
                }}/>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="row">
              <div className="col-sm-6">
                <a href="shifts">
                  <button style={{width: '90%'}} className="btn btn-primary center-block">
                    Schedule
                  </button>
                </a>
              </div>
              <div className="col-sm-6">
                <a href="shifts/swap">
                  <button style={{width: '90%'}} className="btn btn-primary center-block">
                    Swaps & Covers
                  </button>
                </a>
              </div>

            </div>
            <div className="row widget">
              <h3>Queue status</h3>
              <QueueStatus enabled={enabled}
                numRequests={numRequests}
                waitTime={waitTime}
                refresh={this.refreshQueueStatus}
                api={api} />
            </div>
            <div className="row widget">
              <h3>Active helpers</h3>
              <ActiveHelpers helpers={this.state.helpers}
                refresh={this.refreshActiveHelpers} />
            </div>
            <div className="row widget">
              <img width="100%" src="/img/LairMap.png" />
            </div>
          </div>
        </div>
     </div>
    );
  }
});
