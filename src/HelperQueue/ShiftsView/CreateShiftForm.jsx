'use strict';

var React = require('react/addons');
var moment  = require("moment-timezone");
var _       = require("underscore");

var api = require('../../shared/cs198.js');                                                                                                 

module.exports = React.createClass({
  mixins: [React.addons.LinkedStateMixin],

  displayName: 'CreateShiftForm',

  // Return the options to pass to shifts#create
  getSubmitParams: function() {
    var date = this.state.date
    var time = this.state.time

    var start_time = moment(date + " " + time).tz(this.props.timezone).format();
    // convert hours to minutes
    var duration = parseInt(this.state.hours) * 60;
    var repeat = this.state.repeat;

    if (this.state.bulk) {
      var people_ids = this.state.people
      return {
        shifts: people_ids.split("\n").map(function(person_id) {
          return {
            start_time: start_time,
            duration_minutes: duration,
            sunetid: person_id,
            repeat: repeat
          };
        })
      }
    } else {
      var person_id = this.state.people
      return {
        shifts: [{
          start_time: start_time,
          duration_minutes: duration,
          sunetid: person_id,
          repeat: repeat
        }]
      };
    }
  },

  handleSubmit: function(e) {
    e.preventDefault();
    this.setState({ error: false, message: null });

    var opts = this.getSubmitParams();
    api.Lair.Shifts.create(opts).then(result => {
      this.props.submitCallback();
      var numCreated = -1;
      if (result.data instanceof Array) {
        numCreated = result.data.length;
      }
      this.setState({ error: false, message: `Success! ${numCreated} shifts created.` });
    }).catch(err => {
      this.setState({ error: true, message: err.error}); 
    });
  },

  toggleBulk: function(e) {
    e.preventDefault();
    this.setState({ bulk: !this.state.bulk });
  },

  getInitialState: function() {
    var earliest = moment().format("YYYY-MM-DD");
    return {
      people: '',
      date: earliest,
      time: '18:00',
      hours: 2,
      repeat: 10,
      bulk: false,
    };
  },

  render: function() {
    var earliest = moment().tz(this.props.timezone).format("YYYY-MM-DD");
    var bulkToggleText, personInput, startAtText;
    if (this.state.bulk) {
      bulkToggleText = "Create individually...";
      personInput = <textarea className="form-control create-shift-person"
                  valueLink={this.linkState('people')} placeholder="People (newline-delimited)" />;
      startAtText = "These shifts will start at";
    } else {
      bulkToggleText = "Create bulk...";
      personInput = <input className="form-control create-shift-person"
                 valueLink={this.linkState('people')} type="text" placeholder="Person" />;
      startAtText = "'s shift starts at";
    }

    var message = null;
    if (this.state.message) {
      if (this.state.error) {
        message = <p className="errors bg-danger">{this.state.message}</p>;
      } else {
        message = <p className="success-message bg-success">{this.state.message}</p>;
      }
    }

    return (
      <div className="create-shift-form">
        {message}
        <a href="#" onClick={this.toggleBulk}>{bulkToggleText}</a>
        <form className="inline-form" onSubmit={this.handleSubmit}>
          {personInput}
          <span>{startAtText}</span>
          <input type="date" name="start-date" min={earliest} valueLink={this.linkState('date')}/>
          <input type="time" name="start-time" valueLink={this.linkState('time')}/>
          <span>for</span>
          <input className="form-control form-small-number" type="number" valueLink={this.linkState('hours')}
               min="1" />
          <span>hours, repeating</span>
          <input className="form-control form-small-number" type="number" valueLink={this.linkState('repeat')}
               min="1"/>
          <span>times</span>
          <input className="btn btn-primary" type="submit" />
        </form>
      </div>
    );
  }
});
