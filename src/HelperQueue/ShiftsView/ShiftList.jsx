'use strict';

var React   = require('react');
var moment  = require("moment-timezone");
var _       = require("underscore");

var api = require('../../shared/cs198.js');

module.exports = React.createClass({
  getInitialState: function() {
    return { toDelete: null };
  },

  deleteShift: function(e) {
    e.preventDefault();
    var shift = this.state.toDelete;

    api.Lair.Shifts.destroy(shift.id).then(() => {
      var fullName = shift.person.first_name + " " + shift.person.last_name;
      var shift_time = moment(shift.start_time).format('h:mm A');
      var shift_date = moment(shift.start_time).format('dddd MMMM D, YYYY');
      var message = fullName + "'s shift on " + shift_date + " at " +
        shift_time + " has been deleted.";
      alert(message);
      this.props.callback();
      this.setState({ toDelete: null });
    });
  },
  deleteRegularShifts: function(e) {
    e.preventDefault();
    var person = this.state.toDelete.person;
    var fullName = person.first_name + " " + person.last_name;
    var confirmed = confirm("Are you sure you want to delete all of " +
                fullName + "'s regular shifts?");
    if (!confirmed) return;

    api.Lair.Shifts.destroy(person.id, true).then(() => {
      var message = "All of " + fullName + "'s future regular shifts have been deleted.";
      alert(message);
      this.props.callback();
      this.setState({ toDelete: null });
    });
  },

  cancelDelete: function(e) {
    e.preventDefault();
    this.setState({ toDelete: null });
  },

  handleDelete: function(shift) {
    return e => {
      e.preventDefault();
      this.setState({ toDelete: shift });
    };
  },

  render: function() {
    var elems;
    if (this.props.shifts.length === 0) {
      elems = [
        <h3>{this.props.title}</h3>,
        <p className="shift-list-slot">No shifts</p>
      ];
    } else {
      var timezone = this.props.timezone;
      var groupedShifts = _.groupBy(this.props.shifts, function(shift) {
        return moment(shift.start_time).tz(timezone).format('h:mm A');
      });

      var shiftElems = _.map(groupedShifts, (shifts, time) => {
        var shiftListElems = _.map(shifts, shift => {
          if (this.state.toDelete && this.state.toDelete.id === shift.id) {
            var fullName = shift.person.first_name + " " + shift.person.last_name;
            var deletionButtons = [
              <button className="btn btn-default" onClick={this.cancelDelete}>
                Cancel
              </button>,
              <button className="btn btn-primary" onClick={this.deleteShift}>
                Delete {fullName}'s shift
              </button>
            ];
            if (shift.regular_shift) {
              deletionButtons.push(
                <button className="btn btn-info"
                  onClick={this.deleteRegularShifts}>
                  Delete all of {fullName}'s regular shifts
                </button>
              );
            }
            return <li>
              {deletionButtons}
            </li>;
          } else {
            var shiftDisplayElems = [];
            if (this.props.editable) {
              shiftDisplayElems.push(
                <a href="#" onClick={this.handleDelete(shift)}>
                  <span className="glyphicon glyphicon-remove"
                    aria-label="Delete shift"></span>
                </a>
              );
            }
            var displayName = shift.person.first_name + " ";
            if (this.props.abbreviateLastNames && shift.person.last_name.length > 0) {
              displayName += shift.person.last_name.charAt(0) + ".";
            } else {
              displayName += shift.person.last_name;
            }

            if (this.props.editable) {
              displayName +=  " (" + shift.person.sunetid + ")";
            }

            var className = this.props.editable && shift.regular_shift ? "regular-shift" : "";
            shiftDisplayElems.push(<span className={className}>{displayName}</span>);

            return <li>{shiftDisplayElems}</li>;
          }
        });

        return (
          <li><h4>{time}</h4><ul className="shift-list-slot">{shiftListElems}</ul></li>
        );
      });

      elems = [
        <h3>{this.props.title}</h3>,
        <ul>{shiftElems}</ul>
      ];
    }

    return (
      <div className="shift-list widget col-sm-1">
        {elems}
      </div>
    );
  }
});
