'use strict';

var React   = require('react');
var moment  = require("moment-timezone");
var _       = require("underscore");

var ShiftCalendar = require('./ShiftCalendar.jsx');

module.exports = (function(timezone) {
  return React.createClass({
    render: function() {
      return (
        <div className="shifts-view">
          <header className="row title">
            <h1>Create Shifts</h1>
          </header>
          <div className="row">
            <ShiftCalendar editable={true} timezone={timezone} />
          </div>
        </div>
      );
    }
  });
});
