'use strict';

var React   = require('react');
var moment  = require("moment-timezone");
var _       = require("underscore");

var ShiftList = require('./ShiftList.jsx');
var CreateShiftForm = require('./CreateShiftForm.jsx');
var api = require('../../shared/cs198.js');                                                                                                 

module.exports = React.createClass({
  contextTypes: {
    router: React.PropTypes.func.isRequired
  },

  changeWeek: function(forward) {
    return e => {
      e.preventDefault();
      var weekStart = moment(this.state.weekStart);
      if (forward) {
        weekStart.add(1, "week");
      } else {
        weekStart.subtract(1, "week");
      }
      this.setState({ weekStart: weekStart }, this.refreshState);
    };
  },

  refreshState: function() {
    this.refreshShifts();
  },

  refreshShifts: function() {
    var opts = {
      after: this.state.weekStart.format(),
      before: moment(this.state.weekStart).endOf('week').format()
    };

    api.Lair.Shifts.index(opts).then(result => {
      this.setState({ shifts: result.data });
    });
  },

  componentDidMount: function() {
    this.refreshState();
  },

  getInitialState: function() {
    var { router } = this.context;
    var after = router.getCurrentQuery().after ?
      moment(router.getCurrentQuery().after) : moment();
    var weekStart = after.tz(this.props.timezone).startOf('week');
    return {
      shifts: null,
      weekStart: weekStart
    };
  },
  
  render: function() {
    if (this.state.shifts === null) {
      return <div>Loading shifts...</div>;
    }

    var elems = [];

    if (this.props.editable) {
      elems.push(
        <div className="row col-xs-12">
          <h2>Create shift</h2>
          <CreateShiftForm timezone={this.props.timezone}
            submitCallback={this.refreshState}/>
        </div>
      );
    }

    var shiftListElems = [];

    var day = 0;

    // generates filter function used in the loop below
    var shiftBetween = function(startTime, endTime) {
      return function(shift) {
        return moment(shift.start_time).isBetween(startTime, endTime);
      };
    };
    while (day < 7) {
      var startTime = moment(this.state.weekStart).add(day, 'day');
      var endTime = moment(startTime).add(1, 'day');
      var title = startTime.format('ddd - M/D');
      var shifts = _.filter(this.state.shifts, shiftBetween(startTime, endTime));

      shiftListElems.push(<ShiftList title={title} shifts={shifts}
        abbreviateLastNames={this.props.abbreviateLastNames}
        editable={this.props.editable}
        timezone={this.props.timezone}
        callback={this.refreshState} />);
      ++day;
    }

    var dateFormat = 'M/D/YY';
    var weekStart = moment(this.state.weekStart).format(dateFormat);
    var weekEnd = moment(this.state.weekStart).add(6, 'days').format(dateFormat);


    elems.push(
      <div className="row">
        <div className="col-xs-8">
          <h2>
            <span className="heading-text">Shift schedule</span>
            <span className="heading-text dim-text sub-heading">
              ({weekStart}–{weekEnd})
            </span>
            <div>
              <button className="btn btn-default"
                onClick={this.changeWeek(false)}>
                <span className="glyphicon glyphicon-chevron-left"
                  aria-label="Previous week"></span>
              </button>
              <button className="btn btn-default"
                onClick={this.changeWeek(true)}>
                <span className="glyphicon glyphicon-chevron-right"
                  aria-label="Next week"></span>
              </button>
            </div>
          </h2>
        </div>
        <div className="col-xs-2 col-xs-offset-2">
          <a href="shifts/swap">
            <button className="btn btn-primary center-block">
              Swaps & Covers
            </button>
          </a>
        </div>
      </div>
    );

    if (this.props.editable) {
      elems.push(
        <div><p>
          <span className="regular-shift">Bolded names</span> mean regular shifts.
        </p></div>
      );
    }

    elems.push(
      <div className="row col-xs-12 seven-cols">{shiftListElems}</div>
    );

    return (
      <div className="shift-calendar col-xs-12">
        {elems}
      </div>
    );
  }
});
