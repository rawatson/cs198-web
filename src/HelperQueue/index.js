'use strict';

module.exports = {
    HelperView: require('./HelperView/HelperView.jsx'),
    SignupView: require('./SignupView/SignupView.jsx'),
    ShiftsView: require('./ShiftsView/ShiftsView.jsx'),
    CreateShiftsView: require('./ShiftsView/CreateShiftsView.jsx'),
    SwapShiftsView: require('./SwapShiftsView/SwapShiftsView.jsx'),
    SelectSwapType: require('./SwapShiftsView/SelectSwapType.jsx'),
    CoverShiftView: require('./SwapShiftsView/CoverShiftView.jsx'),
    PermanentSwapView: require('./SwapShiftsView/PermanentSwapView.jsx')
};
