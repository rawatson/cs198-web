var gulp = require('gulp');
var gutil = require('gulp-util');
var connect = require('gulp-connect');
var browserify = require('browserify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var react = require('gulp-react');
//var sass = require('gulp-sass');
var babelify = require('babelify');
var compress = require('compression');
var envify = require('envify');
require('dotenv').load();


var chalk = require('chalk');
var _ = require('underscore');

function browserifyBundler(args) {
  args = args || {};
  args = _.extend(args, {
    basedir: __dirname,
    debug: true
  });
  return browserify('./src/App.jsx', args)
    .transform(envify)
    .transform(babelify.configure({"optional": ["es7.classProperties"]}));
}

function browserifyBuild(bundle) {
  return bundle
    .pipe(source('bundle.js')) // gives streaming vinyl file object
    .pipe(buffer()) // convert from streaming to buffered vinyl file object
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./public/js/'));
}

gulp.task('browserify', function() {
  return browserifyBuild(browserifyBundler().bundle());
});

gulp.task('sass', function () {
  return null;
  //return gulp.src('src/scss/app.scss')
  //  .pipe(sass({sourcemapPath: '../src/scss'}))
  //  .pipe(gulp.dest('public/css'));
});

gulp.task('sass-log', function () {
  return gulp.src('src/scss/app.scss')
    .pipe(sass({sourcemapPath: '../src/scss'}))
    .on('error', function (err) { console.log(chalk.bgRed("Sass error:") + " " + err.message); })
    .pipe(gulp.dest('public/css'));
});

gulp.task('watch-js',  function() {
  var failed = false;
  var firstRun = true;
  var successHandler = function(bytes) {
    if (firstRun) {
      console.log(chalk.cyan("JS compile finished"));
      firstRun = false;
    } else if (failed) {
      failed = false; // Our error has already been printed, so just unset the error bit
    } else {
      console.log(chalk.bold.green("Browserify rebuild successful"));
    }
  }
  var errorHandler = function(err) {
    failed = true;
    console.log(chalk.bold.red("Browserify error: ") + err.message);
    this.emit('end');
  };

  var bundler = watchify(browserifyBundler(watchify.args));
  bundler.on('update', rebundle);
  bundler.on('bytes', successHandler);

  function rebundle() {
    if (!firstRun)
      console.log(chalk.cyan('JS changes detected, waiting for rebuild...'));
    else
      console.log(chalk.cyan('JS compile starting...'));
    return browserifyBuild(bundler.bundle()
      .on('error', errorHandler))
  }

  return rebundle();
});

gulp.task('watch-css', function() {
  var watcher = gulp.watch('src/scss/**/*.scss', ['sass-log']);
  watcher.on('change', function(event) {
    console.log(chalk.yellow('SASS change detected: ') +
                'File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  return watcher;
});

gulp.task('watch', ['watch-css', 'watch-js']);


gulp.task('connect', function() {
  connect.server({
    root: './public',
    fallback: './public/index.html',
    port: 5000,
    middleware: function(connect, opt) {
      return [compress()];
    }
  });
});

gulp.task('lint', function() {
  return gulp.src('./src/**/*.{jsx,js}')
    .pipe(react())
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('serve', ['sass', 'connect', 'watch'], function() {
  var msg = chalk.bold.green('Initialization complete:');
  msg += ' the web frontend is now running!';
  console.log(msg);
});

gulp.task('build', ['browserify', 'sass']);
gulp.task('s', ['serve']);
gulp.task('check', ['lint']);
gulp.task('default', ['check']);
